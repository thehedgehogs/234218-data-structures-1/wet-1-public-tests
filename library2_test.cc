#include "library2.h"

#include <gsl/gsl>
#include <gtest/gtest.h>

class Library2Test : public ::testing::Test {
    protected:
        void SetUp() override {
            management = Init(num_of_companies);
            ASSERT_NE(management, nullptr);
            ASSERT_EQ(AddEmployee(management, employee_id, company_id, employee_grade), SUCCESS);
            out_value = not_set;
            out_employees_count = not_set;
            out_employer_id = not_set;
            out_salary = not_set;
            out_grade = not_set;
        }

        void TearDown() override {
            Quit(&management);
            ASSERT_EQ(management, nullptr);
        }

        gsl::owner<void *> management{nullptr};
        static constexpr int num_of_companies{10};
        static constexpr int company_id{1};
        static constexpr int company_value{1};
        static constexpr int company_employees_count{1};
        static constexpr int no_such_company_id{num_of_companies + 1};
        static constexpr int employee_id{1};
        static constexpr int no_such_employee_id{employee_id + 1};
        static constexpr int employee_salary{1};
        static constexpr int employee_grade{1};
        static constexpr int not_set{-1};
        int out_value{not_set};
        int out_employees_count{not_set};
        int out_employer_id{not_set};
        int out_salary{not_set};
        int out_grade{not_set};
};

// see https://google.github.io/googletest/

TEST(InitAndQuit, verify_init_and_quit) {
    void *ds = Init(0);
    EXPECT_EQ(ds, nullptr);

    ds = Init(-1);
    EXPECT_EQ(ds, nullptr);

    ds = Init(1);
    EXPECT_NE(ds, nullptr);

    Quit(&ds);
    EXPECT_EQ(ds, nullptr);
}

TEST_F(Library2Test, AddEmployee_bad_arguments) {
    // verify invalid inputs
    EXPECT_EQ(AddEmployee(nullptr, no_such_employee_id, company_id, 1), INVALID_INPUT);
    EXPECT_EQ(AddEmployee(management, -1, company_id, 1), INVALID_INPUT);
    EXPECT_EQ(AddEmployee(management, no_such_employee_id, -1, 1), INVALID_INPUT);
    EXPECT_EQ(AddEmployee(management, no_such_employee_id, company_id, -1), INVALID_INPUT);
    EXPECT_EQ(AddEmployee(management, 0, company_id, 1), INVALID_INPUT);
    EXPECT_EQ(AddEmployee(management, no_such_employee_id, 0, 1), INVALID_INPUT);

    // already exists (added in the fixture setup)
    EXPECT_EQ(AddEmployee(management, employee_id, company_id, 0), FAILURE);

    // cannot add employee to a non-existent company
    EXPECT_EQ(AddEmployee(management, no_such_employee_id, no_such_company_id, 1), INVALID_INPUT);
    // cannot have two employees of the same id
    EXPECT_EQ(AddEmployee(management, employee_id, company_id, 1), FAILURE);
    EXPECT_EQ(AddEmployee(management, no_such_employee_id, company_id, 1), SUCCESS);
    const int newly_added{no_such_employee_id};
    EXPECT_EQ(AddEmployee(management, newly_added, company_id, 1), FAILURE);
    EXPECT_EQ(AddEmployee(management, newly_added + 1, company_id, 0), SUCCESS);
}

TEST_F(Library2Test, RemoveEmployee_bad_arguments) {
    // verify invalid inputs
    EXPECT_EQ(RemoveEmployee(nullptr, 1), INVALID_INPUT);
    EXPECT_EQ(RemoveEmployee(management, 0), INVALID_INPUT);
    EXPECT_EQ(RemoveEmployee(management, -1), INVALID_INPUT);

    EXPECT_EQ(RemoveEmployee(management, no_such_employee_id), FAILURE);
    EXPECT_EQ(RemoveEmployee(management, employee_id), SUCCESS);

    const int newly_removed{employee_id};
    EXPECT_EQ(RemoveEmployee(management, newly_removed), FAILURE);
}

TEST_F(Library2Test, CompanyValue_bad_arguments) {
    // verify invalid inputs
    EXPECT_EQ(CompanyValue(nullptr, company_id), INVALID_INPUT);
    EXPECT_EQ(CompanyValue(management, no_such_company_id), INVALID_INPUT);
    EXPECT_EQ(CompanyValue(management, 0), INVALID_INPUT);
    EXPECT_EQ(CompanyValue(management, company_id), SUCCESS);
    EXPECT_EQ(CompanyValue(management, num_of_companies), SUCCESS);
}

TEST_F(Library2Test, PromoteEmployee_bad_arguments) {
    const int bump_grade{1};
    EXPECT_EQ(PromoteEmployee(nullptr, employee_id, bump_grade), INVALID_INPUT);
    EXPECT_EQ(PromoteEmployee(management, 0, bump_grade), INVALID_INPUT);
    EXPECT_EQ(PromoteEmployee(management, -1, bump_grade), INVALID_INPUT);
    EXPECT_EQ(PromoteEmployee(management, employee_id, bump_grade), SUCCESS);
    EXPECT_EQ(PromoteEmployee(management, employee_id, 0), SUCCESS);
    EXPECT_EQ(PromoteEmployee(management, employee_id, -1), SUCCESS);
    EXPECT_EQ(PromoteEmployee(management, no_such_employee_id, bump_grade), FAILURE);
}

TEST_F(Library2Test, AcquireCompany_bad_arguments) {
    const int second_company{2};
    const double factor{2};

    EXPECT_EQ(AcquireCompany(nullptr, company_id, second_company, factor), INVALID_INPUT);
    EXPECT_EQ(AcquireCompany(management, 0, second_company, factor), INVALID_INPUT);
    EXPECT_EQ(AcquireCompany(management, -1, second_company, factor), INVALID_INPUT);
    EXPECT_EQ(AcquireCompany(management, num_of_companies + 1, second_company, factor), INVALID_INPUT);
    EXPECT_EQ(AcquireCompany(management, company_id, 0, factor), INVALID_INPUT);
    EXPECT_EQ(AcquireCompany(management, company_id, -1, factor), INVALID_INPUT);
    EXPECT_EQ(AcquireCompany(management, company_id, num_of_companies + 1, factor), INVALID_INPUT);
    EXPECT_EQ(AcquireCompany(management, company_id, company_id, factor), INVALID_INPUT);
    EXPECT_EQ(AcquireCompany(management, company_id, second_company, 0), INVALID_INPUT);
    EXPECT_EQ(AcquireCompany(management, company_id, second_company, -0.00000001), INVALID_INPUT);

    EXPECT_EQ(AcquireCompany(management, company_id, second_company, -0.00000001), INVALID_INPUT);
}

TEST_F(Library2Test, EmployeeSalaryIncrease_bad_arguments) {
    const int salary_increase{1};
    EXPECT_EQ(EmployeeSalaryIncrease(nullptr, employee_id, salary_increase), INVALID_INPUT);
    EXPECT_EQ(EmployeeSalaryIncrease(management, 0, salary_increase), INVALID_INPUT);
    EXPECT_EQ(EmployeeSalaryIncrease(management, -1, salary_increase), INVALID_INPUT);
    EXPECT_EQ(EmployeeSalaryIncrease(management, employee_id, 0), INVALID_INPUT);
    EXPECT_EQ(EmployeeSalaryIncrease(management, employee_id, -1), INVALID_INPUT);
    EXPECT_EQ(EmployeeSalaryIncrease(management, employee_id, salary_increase), SUCCESS);
    EXPECT_EQ(EmployeeSalaryIncrease(management, no_such_employee_id, salary_increase), FAILURE);
}

TEST_F(Library2Test, SumOfBumpGradeBetweenTopWorkersByGroup_bad_arguments) {
    const int m{1};
    EXPECT_EQ(SumOfBumpGradeBetweenTopWorkersByGroup(nullptr, company_id, m), INVALID_INPUT);
    EXPECT_EQ(SumOfBumpGradeBetweenTopWorkersByGroup(management, -1, m), INVALID_INPUT);
    EXPECT_EQ(SumOfBumpGradeBetweenTopWorkersByGroup(management, num_of_companies + 1, m), INVALID_INPUT);
    EXPECT_EQ(SumOfBumpGradeBetweenTopWorkersByGroup(management, company_id, 0), INVALID_INPUT);
    EXPECT_EQ(SumOfBumpGradeBetweenTopWorkersByGroup(management, company_id, -1), INVALID_INPUT);
    EXPECT_EQ(SumOfBumpGradeBetweenTopWorkersByGroup(management, company_id, m), FAILURE);
}

TEST_F(Library2Test, AverageBumpGradeBetweenSalaryByGroup_bad_arguments) {
    const int lower_salary{1};
    const int higher_salary{10};
    EXPECT_EQ(AverageBumpGradeBetweenSalaryByGroup(nullptr, company_id, lower_salary, higher_salary), INVALID_INPUT);
    EXPECT_EQ(AverageBumpGradeBetweenSalaryByGroup(management, -1, lower_salary, higher_salary), INVALID_INPUT);
    EXPECT_EQ(AverageBumpGradeBetweenSalaryByGroup(management, num_of_companies + 1, lower_salary, higher_salary),
              INVALID_INPUT);
    EXPECT_EQ(AverageBumpGradeBetweenSalaryByGroup(management, company_id, lower_salary, -1), INVALID_INPUT);
    EXPECT_EQ(AverageBumpGradeBetweenSalaryByGroup(management, company_id, -1, higher_salary), INVALID_INPUT);
    EXPECT_EQ(AverageBumpGradeBetweenSalaryByGroup(management, company_id, lower_salary + higher_salary, higher_salary),
              INVALID_INPUT);
    EXPECT_EQ(AverageBumpGradeBetweenSalaryByGroup(management, company_id, lower_salary, higher_salary), FAILURE);
    EXPECT_EQ(AverageBumpGradeBetweenSalaryByGroup(management, 0, lower_salary, higher_salary), FAILURE);
    EXPECT_EQ(AverageBumpGradeBetweenSalaryByGroup(management, 2, 0, 0), FAILURE);
    // no matching employees
    EXPECT_EQ(AverageBumpGradeBetweenSalaryByGroup(management, company_id, 100, 100), FAILURE);
    EXPECT_EQ(AverageBumpGradeBetweenSalaryByGroup(management, 0, 1000, 1000), FAILURE);
}

TEST(ExamplesFromInstructions, page_4_acquisitions) {
    gsl::owner<void *> management{Init(6)};
    ASSERT_NE(management, nullptr);

    ASSERT_EQ(AcquireCompany(management, 3, 4, 1), SUCCESS);
    ASSERT_EQ(AcquireCompany(management, 3, 5, 1), SUCCESS);
    ASSERT_EQ(AcquireCompany(management, 5, 4, 1), INVALID_INPUT);

    Quit(&management);
    ASSERT_EQ(management, nullptr);
}

// required static definitions (C++11 is not C++17 :/)
constexpr int Library2Test::company_id;
constexpr int Library2Test::company_value;
constexpr int Library2Test::company_employees_count;
constexpr int Library2Test::no_such_company_id;
constexpr int Library2Test::employee_id;
constexpr int Library2Test::no_such_employee_id;
constexpr int Library2Test::employee_salary;
constexpr int Library2Test::employee_grade;
constexpr int Library2Test::not_set;
